use Mix.Config

config :mailbox_web, namespace: MailboxWeb

config :mailbox_web, MailboxWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: MailboxWeb.ErrorView, accepts: ~w(json)]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :mailbox_web, :generators, context_app: :mailbox

import_config "#{Mix.env()}.exs"
