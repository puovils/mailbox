use Mix.Config

config :mailbox_web, MailboxWeb.Endpoint,
  load_from_system_env: true,
  url: [host: "example.com", port: 80]

import_config "prod.secret.exs"
