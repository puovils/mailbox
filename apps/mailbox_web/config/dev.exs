use Mix.Config

config :mailbox_web, MailboxWeb.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: []

config :mailbox_web, :auth,
  username: "dev",
  password: "dev",
  realm: "Mailbox Dev"
