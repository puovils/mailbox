use Mix.Config

config :mailbox_web, :auth,
  username: "admin",
  password: "secret_pass",
  realm: "Mailbox"
