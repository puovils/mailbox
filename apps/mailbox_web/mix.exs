defmodule MailboxWeb.Mixfile do
  use Mix.Project

  def project do
    [
      app: :mailbox_web,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: [],
      deps: deps()
    ]
  end

  def application do
    [
      mod: {MailboxWeb.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:mailbox, in_umbrella: true},
      {:phoenix, "~> 1.3.2"},
      {:cowboy, "~> 1.0"},
      {:basic_auth, "~> 2.2.2"},
      {:mox, "~> 0.3", only: :test}
    ]
  end
end
