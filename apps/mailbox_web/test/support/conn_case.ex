defmodule MailboxWeb.ConnCase do
  use ExUnit.CaseTemplate
  use Phoenix.ConnTest

  using do
    quote do
      use Phoenix.ConnTest
      import MailboxWeb.Router.Helpers
      import Mox
      @endpoint MailboxWeb.Endpoint
    end
  end

  setup _tags do
    auth_header = "Basic " <> Base.encode64("test:test")

    conn =
      Phoenix.ConnTest.build_conn()
      |> put_req_header("authorization", auth_header)
      |> assign(:mailbox, MailboxMock)

    {:ok, conn: conn}
  end
end
