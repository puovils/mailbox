defmodule MailboxWeb.MessageControllerTest do
  use MailboxWeb.ConnCase
  alias Mailbox.Message

  test "get single message by id", %{conn: conn} do
    MailboxMock
    |> expect(:get, fn "1" -> {:ok, %Message{uid: "1"}} end)
    |> expect(:get, fn "non-existing" -> {:error, :not_found} end)

    json =
      conn
      |> get("/message/1")
      |> json_response(:ok)

    assert json["uid"] == "1"

    json =
      conn
      |> get("/message/non-existing")
      |> json_response(:not_found)

    assert json["error"] =~ "not found"
  end

  test "mark message as read", %{conn: conn} do
    MailboxMock
    |> expect(:mark_as_read, fn "1" -> :ok end)

    conn
    |> put("/message/1/read")
    |> response(:created)
  end

  test "mark message as archived", %{conn: conn} do
    MailboxMock
    |> expect(:mark_as_archived, fn "1" -> :ok end)

    conn
    |> put("/message/1/archive")
    |> response(:created)
  end

  test "pass paginantion and filter args to Mailbox.list", %{conn: conn} do
    MailboxMock
    |> expect(:list, fn 0, 5, [is_read: nil, is_archived: nil] -> {:ok, []} end)
    |> expect(:list, fn 1, 2, [is_read: true, is_archived: true] -> {:ok, []} end)

    conn
    |> get("/message")
    |> json_response(:ok)

    conn
    |> get("/message?offset=1&limit=2&read=true&archived=true")
    |> json_response(:ok)
  end

  test "reject invalid auth credentials", %{conn: conn} do
    conn
    |> put_req_header("authorization", "Basic " <> Base.encode64("invalid"))
    |> get("/")
    |> response(:unauthorized)
  end
end
