defmodule MailboxWeb do
  def controller do
    quote do
      use Phoenix.Controller, namespace: MailboxWeb
      import Plug.Conn
      import MailboxWeb.Router.Helpers
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/mailbox_web/templates",
        namespace: MailboxWeb

      import Phoenix.Controller, only: [get_flash: 2, view_module: 1]

      import MailboxWeb.Router.Helpers
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
