defmodule MailboxWeb.MessageController do
  use MailboxWeb, :controller
  alias Mailbox.Message

  def list(conn, params, mailbox) do
    offset = to_int(params["offset"], 0)
    limit = to_int(params["limit"], 5)
    filter_read = to_bool(params["read"], nil)
    filter_archived = to_bool(params["archived"], nil)

    {:ok, messages} =
      mailbox.list(offset, limit, is_read: filter_read, is_archived: filter_archived)

    json(conn, messages)
  end

  def get(conn, params, mailbox) do
    case mailbox.get(params["id"]) do
      {:ok, message} ->
        json(conn, message)

      {:error, :not_found} ->
        json(put_status(conn, :not_found), %{error: "message not found"})
    end
  end

  def read(conn, params, mailbox) do
    mailbox.mark_as_read(params["id"])
    send_resp(conn, :created, "")
  end

  def archive(conn, params, mailbox) do
    mailbox.mark_as_archived(params["id"])
    send_resp(conn, :created, "")
  end

  def import(conn, json, mailbox) do
    json["messages"]
    |> Enum.map(&to_message/1)
    |> Enum.each(&mailbox.add/1)

    send_resp(conn, :created, "")
  end

  def action(conn, _) do
    mailbox = conn.assigns[:mailbox] || Mailbox
    args = [conn, conn.params, mailbox]
    apply(__MODULE__, action_name(conn), args)
  end

  defp to_message(map) do
    %Message{
      :uid => map["uid"],
      :sender => map["sender"],
      :subject => map["subject"],
      :message => map["message"],
      :time_sent => map["time_sent"],
      :is_read => false,
      :is_archived => false
    }
  end

  defp to_int(string, default) when is_binary(string) do
    case Integer.parse(string) do
      {int, ""} -> int
      _ -> default
    end
  end

  defp to_int(_, default), do: default

  defp to_bool("true", _), do: true
  defp to_bool("false", _), do: false
  defp to_bool(_, default), do: default
end
