defmodule MailboxWeb.Router do
  use MailboxWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", MailboxWeb do
    pipe_through(:api)

    get("/message", MessageController, :list)
    get("/message/:id", MessageController, :get)
    put("/message/:id/read", MessageController, :read)
    put("/message/:id/archive", MessageController, :archive)
    post("/message", MessageController, :import)
  end
end
