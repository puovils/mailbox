defmodule MailboxWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :mailbox_web

  if code_reloading? do
    plug(Phoenix.CodeReloader)
  end

  plug(BasicAuth, use_config: {:mailbox_web, :auth})

  plug(Plug.Logger)

  plug(
    Plug.Parsers,
    parsers: [:json],
    pass: ["*/*"],
    json_decoder: Poison
  )

  plug(MailboxWeb.Router)

  def init(_key, config) do
    if config[:load_from_system_env] do
      port = System.get_env("PORT") || raise "expected the PORT environment variable to be set"
      {:ok, Keyword.put(config, :http, [:inet6, port: port])}
    else
      {:ok, config}
    end
  end
end
