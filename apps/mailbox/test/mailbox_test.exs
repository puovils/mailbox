defmodule MailboxTest do
  use ExUnit.Case
  alias Mailbox.Message

  setup do: Mailbox.clear()

  test "list all messages" do
    msg1 = %Message{uid: "1"}
    msg2 = %Message{uid: "2"}

    given_messages([msg1, msg2])

    assert Mailbox.list(0, 10) == {:ok, [msg1, msg2]}
  end

  test "pagination" do
    msg1 = %Message{uid: "1"}
    msg2 = %Message{uid: "2"}
    msg3 = %Message{uid: "3"}

    given_messages([msg1, msg2, msg3])

    assert Mailbox.list(0, 2) == {:ok, [msg1, msg2]}
    assert Mailbox.list(2, 2) == {:ok, [msg3]}
  end

  test "get by id" do
    msg = %Message{uid: "1"}

    given_messages([msg])

    assert Mailbox.get("1") == {:ok, msg}
    assert Mailbox.get("non existing") == {:error, :not_found}
  end

  test "mark as read" do
    given_messages([%Message{uid: "1", is_read: false}])

    Mailbox.mark_as_read("1")

    {:ok, msg} = Mailbox.get("1")
    assert msg.is_read == true
  end

  test "mark as archived" do
    given_messages([%Message{uid: "1", is_archived: false}])

    Mailbox.mark_as_archived("1")

    {:ok, msg} = Mailbox.get("1")
    assert msg.is_archived == true
  end

  test "filter messages" do
    unread_unarchived = %Message{uid: "1", is_read: false, is_archived: false}
    read_unarchived = %Message{uid: "2", is_read: true, is_archived: false}
    unread_archived = %Message{uid: "3", is_read: false, is_archived: true}
    read_archived = %Message{uid: "4", is_read: true, is_archived: true}

    given_messages([unread_unarchived, read_unarchived, read_archived, unread_archived])

    assert Mailbox.list(0, 4, is_read: true) == {:ok, [read_unarchived, read_archived]}
    assert Mailbox.list(0, 4, is_read: false) == {:ok, [unread_unarchived, unread_archived]}
    assert Mailbox.list(0, 4, is_archived: true) == {:ok, [read_archived, unread_archived]}
    assert Mailbox.list(0, 4, is_archived: false) == {:ok, [unread_unarchived, read_unarchived]}

    assert Mailbox.list(0, 4, is_read: true, is_archived: true) == {:ok, [read_archived]}
    assert Mailbox.list(0, 4, is_read: true, is_archived: false) == {:ok, [read_unarchived]}
    assert Mailbox.list(0, 4, is_read: false, is_archived: true) == {:ok, [unread_archived]}
    assert Mailbox.list(0, 4, is_read: false, is_archived: false) == {:ok, [unread_unarchived]}
  end

  test "don't add duplicate messages" do
    assert Mailbox.add(%Message{uid: "1"}) == :ok
    assert Mailbox.add(%Message{uid: "1"}) == {:error, :already_exists}
  end

  test "clear mailbox" do
    given_messages([%Message{uid: "1"}])

    Mailbox.clear()

    assert Mailbox.list(0, 1) == {:ok, []}
  end

  defp given_messages(messages), do: Enum.each(messages, &Mailbox.add/1)
end
