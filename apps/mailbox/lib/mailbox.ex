defmodule Mailbox do
  @behaviour Mailbox.Api
  use GenServer
  alias Mailbox.Message

  def list(offset, limit, filter \\ []), do: call({:list, offset, limit, filter})
  def get(id), do: call({:get, id})
  def add(message = %Message{}), do: call({:add, message})
  def mark_as_read(id), do: call({:update, id, :is_read, true})
  def mark_as_archived(id), do: call({:update, id, :is_archived, true})

  def start_link, do: GenServer.start_link(__MODULE__, {}, name: __MODULE__)
  def clear, do: call(:clear)

  def init(_opts), do: {:ok, []}

  def handle_call({:list, offset, limit, filter}, _from, state) do
    messages =
      state
      |> Enum.filter(&matches?(&1, filter))
      |> Enum.slice(offset, limit)

    {:reply, {:ok, messages}, state}
  end

  def handle_call({:add, message}, _from, state) do
    case Enum.find(state, nil, &(&1.uid == message.uid)) do
      nil -> {:reply, :ok, state ++ [message]}
      _ -> {:reply, {:error, :already_exists}, state}
    end
  end

  def handle_call({:get, id}, _from, state) do
    case Enum.find(state, nil, &(&1.uid == id)) do
      nil -> {:reply, {:error, :not_found}, state}
      message -> {:reply, {:ok, message}, state}
    end
  end

  def handle_call({:update, id, field, value}, _from, state) do
    state =
      Enum.map(state, fn
        msg = %Message{uid: ^id} -> Map.put(msg, field, value)
        msg -> msg
      end)

    {:reply, :ok, state}
  end

  def handle_call(:clear, _from, _state), do: {:reply, :ok, []}

  defp call(arg), do: GenServer.call(__MODULE__, arg)

  defp matches?(message, filter) do
    nil_or_same?(filter[:is_read], message.is_read) &&
      nil_or_same?(filter[:is_archived], message.is_archived)
  end

  defp nil_or_same?(nil, _), do: true
  defp nil_or_same?(v, v), do: true
  defp nil_or_same?(_, _), do: false
end
