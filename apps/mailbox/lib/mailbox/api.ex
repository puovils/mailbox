defmodule Mailbox.Api do
  @callback list(integer(), integer(), list()) :: {:ok, list()} | {:error, any()}
  @callback get(binary()) :: {:ok, any()} | {:error, any()}
  @callback add(any()) :: :ok | {:error, any()}
  @callback mark_as_read(binary()) :: :ok
  @callback mark_as_archived(binary()) :: :ok
end
