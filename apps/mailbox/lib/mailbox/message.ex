defmodule Mailbox.Message do
  defstruct [:uid, :sender, :subject, :message, :time_sent, :is_read, :is_archived]
end
