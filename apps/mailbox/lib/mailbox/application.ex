defmodule Mailbox.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link(
      [worker(Mailbox, [])],
      strategy: :one_for_one,
      name: Mailbox.Supervisor
    )
  end
end
