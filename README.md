# About

Requirements can be found in [Mailbox API](Mailbox%20API.pdf). I choose [Elixir](https://elixir-lang.org/), 
because it allows fast prototyping, rich standard library, built-in testing framework and in general 
provides pleasant environment :)  

For this early stage I mostly focused on separating logical compotents, so different people or teams can move 
forward with new features independently. In this repository you see two 
[Erlang/OTP](http://erlang.org/doc/design_principles/applications.html) applications:

* `mailbox` contains core domain logic and services. For now it stores messages in memory. But it is 
trivial to implement different storages using [`Mailbox.Api`](apps/mailbox/lib/mailbox/api.ex) behaviour. 
* `mailbox_web` provides REST interface to use `mailbox`. It uses [Pheonix](http://phoenixframework.org/) to 
serve HTTP requests.

# Usage

First of all you need to [install](https://elixir-lang.org/install.html) Elixir. Then checkout code with

```
git clone https://bitbucket.org/puovils/mailbox 
cd mailbox
```

Download dependencies and run dev server.
```
mix deps.get
mix phx.server
```

At this point you shoud have server listening at `http://localhost:4000`. `mailbox_web` uses basic auth, for 
development environment username is `dev` and password `dev`. Because we will be using authorization for each 
request so lets just do this 
```
export AUTH='-u dev:dev'
```

Now we can load sample data:
```
curl $AUTH -X POST http://localhost:4000/message \
    -H "Content-Type: application/json" \
    -d "@apps/mailbox_web/test/messages_sample.json" 
```

Simple listing of all messages
```
curl $AUTH -X GET http://localhost:4000/message
```

To get indivdual message
```
curl $AUTH -X GET http://localhost:4000/message/21
```

Mark message as read
```
curl $AUTH -X PUT http://localhost:4000/message/21/read
```

Mark message as archived
```
curl $AUTH -X PUT http://localhost:4000/message/21/archive
```

Message listing accepts couple parameters. `offset` and `limit` can be used to implement pagination. 
Default `limit` is `5`. In addition you can apply filters. `read` and `archived` can have values `true` or `false`.
If any other value is provided, the filter will be simply ignored.  
```
curl $AUTH -X GET "http://localhost:4000/message?offset=0&limit=2&read=true&archived=true"
```


# Possible next steps

* Persistent storage for messages
* Use [JSON-API](http://jsonapi.org/) or similar alternative
* Better error messages
* Logging & metrics
* Maybe support multiple users?
